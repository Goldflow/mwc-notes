

## install ionic: ##

	npm install -g ionic@beta


## after ionic is installed globally ##
check if installed correctly by using help command
	ionic -h

## creating ionic project ##
to get create ionic 2 project in typescript

	ionic start projectnaam --v2 --ts

## ionic tutorial project ##

to create ionic 2 tutorial project in typescript

this will already give you some files to set you up 

	ionic start project-tutorial-name tutorial --v2 --ts

not bad idea to do this also

	npm install

to make sure all required modules are installed

##   <ion-app></ion-app> ##

inside www map zit index.html

## cordova ##

to implement native features

## Running ionic project ##

to run:

	ionic serve

to run multiple platforms:

	ionic serve --lab


## adding platforms ##

to add a specific platform (support)

	ionic platform add android

same for other platforms

## running on android phone ##

	ionic run android


## ionic view app ##

if you have account at [ http://ionic.io/]( http://ionic.io/)

you can upload project download it to your phone



if you have ionic view app installed



## create boileterplate page: ##

	ionic g page [namePage]

will create html + css + ts file with same name


## navigate to page with Ionic ##

in html, this will navigate to another page: which is defined behind goToPage


	 <button primary [navPush]="goToPage">go To Extra Page</button>


in our class, where goToPage is defined, it is written like:

	
imports for the extrapage:

		import {ExtraPage} from '../extra/extra';
	
inside the class:

	    goToPage = ExtraPage;


## app.ts ##

om iets toe te voegen aan menu

this.pages de pagina toevoegen + bovenaan ook importeren

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Getting Started', component: GettingStartedPage },
      { title: 'List', component: ListPage },
        {title: 'Extra',component: ExtraPage}
    ];

ook vanboven imports wer fixen:

voor de import alias, kijk binnen de klasse naar naam van klasse

  	import {ExtraPage} from './pages/extra/extra';

## theme aanpassen ##

app.variables.scss

hier kan je colors aanpassen

## create a provider ##

to create a (data)provider

willl create map with typescript file with template code to create a http request

	ionic g provider PeopleService


## generate random users ##

	https:randomuser.me/api/?results=10
