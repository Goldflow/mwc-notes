# Responsive Design #

## Bootstrap ##

### Start: ###

Bootstrap CDN
If you don't want to download and host Bootstrap yourself, you can include it from a CDN (Content Delivery Network).

MaxCDN provide CDN support for Bootstrap's CSS and JavaScript. Also include jQuery:

MaxCDN:
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		
		<!-- Latest compiled JavaScript -->
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		

Note	One advantage of using the Bootstrap CDN:
Many users already have downloaded Bootstrap from MaxCDN when visiting another site. As a result, it will be loaded from cache when they visit your site, which leads to faster loading time. Also, most CDN's will make sure that once a user requests a file from it, it will be served from the server closest to them, which also leads to faster loading time.


### general ###

works with a Grid View

generally 12 columns

for example

build grid-view

	.menu 
		{ width:25%; float: left;}
	.content
		{ width:75%; float: right;}


if you build your own css: very important to include the border in the width

	* { box-sizing: border-box; }



### BreakPoint ###

condition in design to see what screen the end user is working on

how? media queries:

@media

for example
	
	@media only screen (max-width:500px) {
			body {
				background-color:lightblue;
			}
		}

you can also check orientation & other parameters 

###  Mobile First  ###

always think of Mobile first (like you did IE first before)



## Skeleton ##

it's only html css but is more or less like bootstrap


much more lightweight & less functionality than boostrap


has utility.css
and all classess have classes start with "u-..."

