# ES6 #

## Slides instructies ##

run lite-server of andere in die map
terwijl het run 

- o => overzicht
- h => hide clutter
- p => extra notes / code oplossingen

## korte intro git ##

Gitflow: techniek om met verschillende branchens samen te werken

iedereen werkt op verschillende branches, en dan alles weer samen mergen

[https://www.atlassian.com/git/](https://www.atlassian.com/git/)

very good guide for git with images etc.


## Recommended resource sites ##

heel toffe site om dingen te leren
heel veel sessies en lectures

[https://frontendmasters.com/](https://frontendmasters.com/)

[https://egghead.io/](https://egghead.io/)

[https://app.pluralsight.com/library/](https://app.pluralsight.com/library/)

## Joeri Van Steen bio ##

###  Twitter Joeri ###

tweet veel toffe links

joeri van steen [https://twitter.com/joerivansteen](https://twitter.com/joerivansteen)

lector gent
started JSValley

### Google IO  ###
was het punt dat hem inspireerde
zowel over native als web kan je alle sessies bekijken

leerde volgende dingen kennen

### Grunt ###
#### verouderd nu  > is nu GULP ####

**Minification:**
gaat alle bestanden bundlen en bv alle code comprimizen naar functies en variables van 1 of 2 letters
Image Optimazation
**Linting:**
best practices afdwingen
**Automatisatie**

### Douglas Crockford ###

een van de guru's
(maker javascript? niet zeker)

heel veel interesante boeken geschreven

#### Crockford on JavaScript - Volume 1: The Early Years ####
heeft een video reeks gedaan over geschidenis van programmeertalen:
[https://www.youtube.com/watch?v=JxAXlJEmNMg](https://www.youtube.com/watch?v=JxAXlJEmNMg)

#### JavaScript: the good parts ####
heeft ook boek geschreven over JavaScript basics is oud

### Nickolas Zakas ###

protogé of Douglas Crockford

#### Professional JavaScript for Web Developers ####
een van de bijbel!

#### Maintainable JavaScript ####
manier van schrijven om beter te kunnen samen werken en leesbaarder code

#### Secrets of the JavaScript Ninja ####
heel toffe javascript libraries

#### in 2014 switched to REACT.JS ####

#### then also ES5/ES6 ####

#### summer 2015: freelance assignment ITP: Smart City ITP ####


## Applicaties & Frameworks recommended ##

### Babel ###

vertalen van ES6 naar javascript? dachtk :D
### SourceTree ###
 (visual git)

### TurboScript package in Atom ###

sneller & effeciënter coderen

bv

- ls+tab  wordt console.log
- rcc (react class) > direct sample code voor react class

## Best Practices door Joeri ##

### hoe leren goeie code te schrijven? ###

kijk naar bestaande packages & libraries en ga lezen in de source code en probeer dat de begrijpen

bv Sass leren:
lees bootstrap source code

### packaging ###
Het is een goed idee om modulair te werken, het is nu meer en meer een trend om zelf je packaging te doen
zelf kiezen welke libraries & frameworks gebruiken

### gebruik niet voor het minste een framework ###
als je enkel het grid systeem gebruikt van Bootstrap
schrijf dan ZELF je GRID systeem of gebruik een framework dat ENKEL GRID doet (zoals bv skeleton)

### scaffolding ###

samenstellen van startprojecten met bv **Yeoman**.

maak je eigen startprojecten met alle benodigde codebases & libraries 

### blijf up to date met je frameworks ###

als je bepaalde frameworks gebruikt, zoals bv Angular of React, **VOLG DE BLOGS**

### DOCUMENTEER UW CODE ###

criteria om software te verkopen


## EcmaScript 6 ##

learn at: [http://www.es-learning.com/](http://www.es-learning.com/)
### European Computer Manufacturer Association ###

was vroeger groep dat fabrikanten samenkwamen en commitee maakten om standaarden te beslissen
check **TC39 Goals**

###  better language ###

### testable ###

### TC39 is proposing features in ES7 ###

see TC39 Notes website

### Browser vendors are working on implementing ES6 ###


## Harmony or ES.Next ##

### Harmony ###
slaat op de container van alle mooie features die vroeger al eens voorgesteld zijn

toekomstige nog niet geimplementeerde features

### ES.NEXT  ###
slaat op de versie van EcmaScript die gerealiseerd **zal worden** op features uit Harmony library

### ES Versie ###
bv ES6

de laatste nieuwe versie met effectief geimplementeerde features


## Tools Compilation ##

### Babel ###

tot en met babel 5 was het gewoon een transpiler

babel 6 voorziet nog meer tools

### Webpack ###
(je hebt ook nog gulp, grunt) maar grunt verouderd

check pluralsight

bundelen van code

### Linting ###
- kan je configureren in je webeditor
- of andere aparte tools
- enforces best practice

### IDE's ###
- Atom (favoriet van Joeri)
- Sublime
- Visual Code

### Documentation: ESDOC & apiDOC ###

generate documentation

### Testing: Ava ###

heel goed naar maintainability > als je later iets aanpast ga je ook direct zien wat er fout liep adhv je tests




# Exercises #


### ES6 Kata ###
Joerie haalde hier inspiratie 
leer ES6 door falende code te fixen

http://es6katas.org/

uiteindelijk doen we alle oefeningen daarop omdat exercises serven problemen gaf


### Gulp ###


install 

npm install -g gulp

### Babel ###

check...

## Variable Hoisting ##

#### zie slides voor betere uitleg dan dit ####

to understand difference between LET & CONST

VAR: gaat alle variablen vinden en bovenaan zetten

joeri:

als variabelen nergens gaat veranderen: CONST

als variabele gaat veranderen: LET (doet geen variablee hoisting


>inspiriatie nickolas zakas high performance javascript

##Execution context: Scope Chain ##

telkens nieuwe functie wordt declareerd, neemt hij scope over van vorige functie

## Block scope exercies ##

geeft false:

 	it('`var` works as usual', () => {
      if (true) {
        let varX = true;
      }
      assert.equal(varX, true);
    });

geeft true:

	 it('`var` works as usual', () => {
      if (true) {
        var varX = true;
      }
      assert.equal(varX, true);
    });


let is block scope (function is ook block scope)
var is function scope

### GEBRUIK VAR NIET MEER ###
als je in ES6 programmeert

#### maar begrijp het wel, wordt wel nog constant gebruikt ####


## Rest Parameters ##

mogelijkheid meerdere parameters mee tegeven

dynamic aantal parameters

**before**

### 'arguments' object ###

	function foo(){ //leave the signature empty
    var output = "";
    for(var i = 0; i < arguments.length; i++){
      output += arguments[i];
    }
    console.log(output); //Logs 'I can haz teh arguments'
	  }
	  foo('I', 'can', 'haz', 'teh', 'arguments');

werkt maart is GEEN ARRAY daarom "rest parameters"

en moesten vroeger dan ook dat omvormen naar een soort van array

	function foo(){
	 //leave the signature empty
    var output = Array.prototype.join.call(arguments, " ");
    console.log(output); //Logs 'I can haz teh arguments'
 	 }
  	 foo('I', 'can', 'haz', 'teh', 'arguments');

Converting to an array:

	Array.prototype.slice.call(arguments)

we gebruiken methode van Array
this van slice slaat op die


**now**

### rest parameters ###

is wel een array en daarbij heb je nu ook heleboel functies beschikbaar


	 function argumenty(name){
	    console.log(name, arguments);
	  }
	  function resty(name, ...other){
	    console.log(name, other);
	  }
	  argumenty("Aaron", "Frost", "Salt Lake City", "Utah");
	  resty("Aaron", "Frost", "Salt Lake City", "Utah");
		
//resty zal het volgende geven:

	Aaron ["Frost", "Salt Lake City", "Utah"]

// dus name variabele (hier Aaron) en dan de "rest" van de parameters dus de "rest parametesr" in een array


### oefening: ###

[http://tddbin.com/#?kata=es6/language/rest/as-parameter](http://tddbin.com/#?kata=es6/language/rest/as-parameter)


## Destructuring ##

gebruikt in bijn alle nieuwste frameworks & libraries
>Destructuring allows you to bind a set of variables to a corresponding set of values anywhere that you can normally bind a value to a single variable.

vroeger als je eigenschap uit een object meerdere keren gebruikt > stak je dat in een lokale variabele om te cachen

	function displayPerson(p){
    var name = p.name;
    var age = p.age;
    //do something with name and age to display them
  	}

nu veel makkelijker hetzelfde doen:µ

	function displayPerson(p){
    let {name, age} = p;
    //do something with name and age to display them
  	}

### new improved - default values###
nog meer advanced voorbeeld:

	  function displayPerson({name = "No Name Provided", age = 0}){
	    //do something with name and age to display them
	  }

hier zijn die parameters wel niet verplicht aanwezig op het object

werd vroeger dan bv gedaan met

	var name = person.name || 'joeri' //als string null is dan dan 'joeri'


### nesting ###

	let {name, age, address: {city, state, zip}} = person;

nu kan je gwn 

	console.log(city)
	//printout: "Brussel"


bij destructuring 

gaat hij heerst naar object opzoeken naar property of functie dat hij daar vind
als hij het daar niet vind, gaat hij het opzoeken in prototype
en dan naar alle objecten die daar over erven

link oefeningen:

[http://tddbin.com/#?kata=es6/language/rest/as-parameter](http://tddbin.com/#?kata=es6/language/rest/as-parameter)

oefeningen oplossingen:

	// 12: destructuring - object
	// To do: make all tests pass, leave the assert lines unchanged!
	
	describe('destructuring objects', () => {

	  it('is simple', () => {
	    const {x} = {x: 1};
	    assert.equal(x, 1);
	  });
	
	  describe('nested', () => {
	    it('multiple objects', () => {
	      const magic = {first: 23, second: 42};
	      const {magic: {second}} = {magic};
	      assert.equal(second, 42);
	    });
	    it('object and array', () => {
	      const {z:[,x]} = {z: [23, 42]};
	      assert.equal(x, 42);
	    });
	    it('array and object', () => {
	      const [,{lang}] = [null, {env: 'browser', lang: 'ES6'}];
	      assert.equal(lang, 'ES6');
	    });
	  });
	  
	  describe('interesting', () => {
	    it('missing refs become undefined', () => {
	      const {z} = {x: 1};
			//variabele namen moeten dezelfde zijn bij destructuring
	      assert.equal(z, void 0);
	    });
	  
	    it('destructure from builtins (string)', () => {
	      const {substr} = '1';
			//omdat een String object substr functie heeft
			//kan die substr functie in de gelijkgenaamde substr functie gestoken worden
	      assert.equal(substr, String.prototype.substr);
	    });
	  });
	
	});




## Default Parameters ##


What Does It Get Me?

- Write Less Code
- Easier To Read
- Improved Predictability


e.g.:

	function test(a = 1, b){ //This is OK
    //Your Code
  	}




## This ##

### verschillende manieren waar this op kan slaan ###

#### 1. this binnen constructor van object verwijst naar instantie van dat object ####

#### 2. binnen een functie verwijst this naar de instantie van die het object waarbij functie wordt aangeroepen ####

hoofdletter moet niet maar om aan te geven is constructor van klasse (voor de lezer van code)

	var Person = function(name) {
		this.name = name;
		//this slaat hier op de instantie van dat object
	}

new sleutelwoord maakt duidelijk aan compilter dat het gaat om constructor

	var joeri = new Person('Joeri');

#### 3. Call/Apply this als eerste param meegeven ####

//nu wordt functie voor elke persoon opnieuw aangemaakt

	var Person = function(name) {
		this.name = name;
		this.showName = function() {
		console.log('name',this.name);
		}
	}

//nu wordt functie maar 1 keer aangemaakt
//maar kan door elk person object gebruikt worden

	var Person = function(name) {
		this.name = name;
	}

	Person.prototype.showName = function() {
		console.log('name',this.name);
		}


soms wil je functie van een ander object gebruiken op een ander object die die functie niet heeft:

	var trixie = new Dog('Trixie');
	
	Person.prototype.showName.call(trixie); //verwacht this en dan eventuele parameters

hier gaat het object trixie ook de functie showname krijgen

.apply bestaat ook maar verwacht array


#### 4. this bij global scope ####

## Arrow Functions ##

aka Fat Arrow Functions aka Lambda Functions


	 var x;
	  x = () => {};     //No parameters, MUST HAVE PARENS
	  x = (val) => {};  //One parameter w/ parens, OPTIONAL
	  x = val => {};    //One parameter w/o parens, OPTIONAL
	  x = (y, z) => {}; //Two or more parameters, MUST HAVE PARENS
	  x = y, z => {};   //Syntax Error: must wrap with parens when using multiple params

## Classes ##

history: al sinds ES4 willen mensen klasses

### precedence: ###

op elke functie zit een prototype (functie is een object dus ook op elk object)

die prototype bevat ook een ``__proto__``
``__proto__`` geeft prototype van de bovenliggende functie
die functie prototype genaamd ``__proto__`` heeft ook terug een eigen ``__proto__``

### niet altijd nodig classes te gebruiken ###

zeker ivm modulair werken en performantie

### zelfs al gebruik je class, blijft een functie ###

### hoe het er vroeger uitzag (JavaScript) ###
 om functie bij klasse te maken

	function Person(name) {
		this.name = name;
		this.showName = function() {
			return this.name;
		}
	}
	
	var joeri = new Person('joeri');
	console.dir(joeri);

beter (gebruikmakend van prototype):

	
	function Person(name) {
		this.name = name;
	}
	
	var joeri = new Person('joeri');
	Person.prototype.showName = function() {
			console.log(this.name);
		}

### effectieve class new syntax ###
(zie slides)

als je van nieuwe class syntax gebruik maakt, weet gwn dat hij achterliggend ook prototypes gebruikt

	const monsterHealth = Symbol();

	  class Monster{
	    constructor(name, health){
	      this.name = name;
	      this[monsterHealth] = health;
	    }
	  }

check: [https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Symbol](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Symbol)

Symbol() genereert unieke hashcode, in bovenstaande code wordt dus die hashcode dus de naam van de variabele, op die manier kan die niet zomaar buiten die scope aangesproken worden

is nog meer ingewikkeld dan dat, maar zoek later op


### Benefits of Classes: ###

- Sugar
- Easier to learn #noobs
- Subclassing easier to learn #noobs
- Subclass built in object: Array, Error, etc
- Code more portable between JS Frameworks




### probeer niet te vlug te overerven, wees kritisch over uw code max 3 of 4 keer overerven ###

als je veel wil overerven mbhv prototypes check nog eens **constructor stealing pattern** + **call pattern**


### Meeste frameworks & libraries gebruiken nu tegenwoordig classes ###


## Collections ##

gebruik WeakMap wanneer je kan zodat references naar dom elementen de garbage collector niet preventen van die objecten te verwijderen uit het geheugen!!

zie slides


## Modules ##

**basiccily importeren van andere javascript files, als klasses in andere javascript files
(geen require meer nodig)**

zoals import in onze de full on typed talen zoals Java

	import React from 'react';

etc.

in de klasse waarvan je importeert:

	exports Default React;


**in babel zal dan alle imports gwn allemaal in 1 file steken!!!**

### export functions & variables ###

zo kan je bv enkel bepaalde functies of variabelen exporteren en de rest hiden

	export function square(x) {
	  return x * x;
	}

dan om te importeren:

	import { square, diag } from 'lib';
	console.log(square(11)); // 121
	console.log(diag(4, 3)); // 5

wil je toch alle functies en variabelen , wel nodig om alias te gebruiken dan


	import * as lib from 'lib';
	console.log(lib.square(11)); // 121
	console.log(lib.diag(4, 3)); // 5
<hr/>
als ze de gevraagde import niet vinden, zal er gezocht worden naar node modules

bv 

	import React from 'react';

zal zoeken in nodemodules naar React map 

leer WebPack kennen die alle modules gaat bundelen in een bundle.js


## Promises ##

used to prevent "callback hell"

[http://callbackhell.com/](http://callbackhell.com/)

> callback functie die op bepaald moment iets teruggeeft, afhankelijk van tijdstip of als een bepaalde taak volbracht is

## ES7 ##

### ECMA7: Async/await ###

Async/await with promises

try catch

### ECMA7 Decorators ###

functionaliteit delen tussen klasses
een functie herbruiken in verschillende klasses of functies

## Generators ##

lossen problemen op waarbij je we code zouden willen pauzeren

ook handig om Asynchroon te werken (hoewel Async in ECMA7 beter is)


	 function *myGen() {
	    //....
	  }

check DWB David Walsh > going Async With ES6 Generators

[https://davidwalsh.name/async-generators](https://davidwalsh.name/async-generators)

**Redux Saga**

## Best Practices - source recommendations - tussendoor gezegd##


>In javascript gebeurt alles in functies, functies zijn first class citizens

### Javascript for Professionals  ###

ebook gratis online beschikbaar

### new function syntax ###
from

	 function(){}

to New function syntax in ES6
 
	() => {}


new return function + single line:

	const fn = () => arguments;
	//only works if the return statement is only one statement/expression

zelfde als:

	const fn = () => {return arguments;}


### ivm linting consistency ###

spreek af met je team welke code conventies en pas die allemaal toe in je Linting software / plugin

recommended by Joeri > eslint-airbnb
> gebruik Linting regels van airbnb, goed coding team


### DO NOT USE W3SCHOOLS ###

is niet officieel van W3 consortium
gwn paar man die naam geregistereerd hebben **maar staat vol met fouten!!!**

[http://www.w3fools.com/](http://www.w3fools.com/)

je kan chrome plugin te installeren om W3schools te verbergen


### niet imperatief programmeren, het is een functionele programeertaal!! ###

een van de redenen waarom er zoveel tegenstanders zijn (check oa smashing magazine)
 van klasses is omdat mensen dan beginnen te coderen/denken in Java ipv Javascript

[https://www.smashingmagazine.com/2014/07/dont-be-scared-of-functional-programming/](https://www.smashingmagazine.com/2014/07/dont-be-scared-of-functional-programming/)

imperatief: zoals c#, java, c++

 check **"hardcore functional programming in javascript"** course

@frontendmasters.com (moeten vragen aan CTG voor ook een shared account)

### Lees "Professional Javascript" Nickolas Lakas MUST READ ###

vooral eerste 50-150 paginas 

### je kan ook multiline programmeren zonder te executen: ###

shift + enter

### meer over dev tools Addy Osmani ###

look up sessions on youtube

### leer goed Dev Tools kennen van google chrome ###
er is bij dev tools knop ook een <> format button 

terug van minified of gecomprimeerde css terug 

heel goed voor debugging, network, performance, styling te checken

### CHECK CHROME CANARY ###

zorg dat je altijd laatste versie geïnstalleerd hebt

telkens laatste snufjes van dev tools included

### leer goed hoe performance & network testing te doen ###

check > [http://www.webpagetest.org/](http://www.webpagetest.org/)


### Angular 1.x vs Angular 2 vs React.JS ###

React.JS zeker beter dan Angular 1.X

behalve als je component based werkt op Angular.JS en met ECMAScript6

check JSValley github account

tussen ReactJS en Angular2 is het moeilijker om te kiezen omdat beiden zelfde aanbieden 

echter Angular2 heeft niet zo'n groot ecosysteem als React.JS

### Leer component based te werken in Angular ###

DOE DEZE CURSUS

https://egghead.io/series/angularjs-application-architecture

en dan DAARNA VERVOLG (geschreven door Joeri ook in EcmaScript 6):

[https://github.com/JSValleybe/Meetup_2016-03-24_Angular_and_ES6_Refactor_Eggly_Project](https://github.com/JSValleybe/Meetup_2016-03-24_Angular_and_ES6_Refactor_Eggly_Project)

Angular 

project op JSVAlley ga je in feite een samengevoegd project geleidelijk aan te refactoren
zie de verschillende commits

een project dat op 't einde dan naar Angular 2 structuur is gekomen


### Meteor ###

alles gebeurt met notificiations
... look it up 


### String interpolatie ###

ipv extra aanhalingstekens en plussen kan je dit doen:

	console.log(Fifteen is $(a+b) and not $(2 * a + b)');

### Progressive Web Applications ###

nieuwste van nieuwste, progressive loading; maar is nog niet veel van te vinden
bundle wordt bij homepage aangemaakt (van alle modules) en dan Async geload

zie Async Loading Support


### check WebPack on PluralSight MUST ###

belangrijk daar mee te leren werken



