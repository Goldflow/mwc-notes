# Sass #
<hr/>



The difference with css is that we can have variable names (starting with $)

Also when working with multiple css files (for example using import), its better combining them all in one, this way you only do one request.


In sass you can organize it in different files & make it compile to a single file (making it load faster)

check this resource: the lesson was based on this:

### [http://sass-lang.com/guide](http://sass-lang.com/guide) ###



## Compiling sass to css ##
command line to compile sass to css to do this:

    sass input.scss output.css

## Nesting sass ##
However you can also compile with koala app

	nav {
		ul {
			margin:0;
			padding:0;
			list-style:none;	
		}
		
		li {
			display:inline-block;
		}
		
		a{
			display:block;
			padding: 6px 12px;
			text-decoration:none;
		}
	
	}


## Creating a partial SCSS ##

In sass you can organize in different files (with partial sass files) & make it compile to a single file (making it load faster)


a kind of component style that you want to reuse in other sass files


filename must always start with _ for example "_partial.scss"


## You can use function-like constructions: ##

scss:

	@mixin border-radius($radius) {
		-webkit-border-radius:$radius;
		-moz-border-radius:$radius;
		-ms-border-radius:$radius;
		-border-radius:$radius;
	}
	
	.box{
		@include border-radius(10px);
	}

css:

	.box {
	  -webkit-border-radius: 10px;
	  -moz-border-radius: 10px;
	  -ms-border-radius: 10px;
	  -border-radius: 10px; }

## you can make calculations ##

scss

	div[role="main"] {
		float:left;
		width:600px/1024px * 100%;
	}

will compile to (css):

	div[role="main"] 
	{
	  float: left;
	  width: 58.59375%; 
	}
## Inheritance ##

scss:

	.message {
		border:1px solid #ccc;
		padding: 10px;
		color:#333;
	}
	
	.success {
		@extend .message;
		border-color:green;
	}
	
	
	.danger {
		@extend .message;
		border-color:red;
	}


css:

	.message, .success, .danger {
	  border: 1px solid #ccc;
	  padding: 10px;
	  color: #333; }
	
	.success {
	  border-color: green; }
	
	.danger {
	  border-color: red; }
