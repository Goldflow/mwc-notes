## Flexbox ##

.flex-container

items negeren hun standaard css en kunnen ook inline worden afhankelijk van grootte scherm

### flex-direction ###
row : autoamtisch van links naar rechts
row-reverse: automatisch alle elementen van rechts naar links

    flex-direction:row-reverse

column: automatisch van boven naar beneden, ongeacht van hun display properties
column-reverse: omgekeerd

	flex-direction:column


### flex-wrap ###

hou de grootte zodat alle items in er in passen, zonder de items aan te passen

	flex-wrap:wrap

de items aanpassen zodat ze passen in de flex-container

	flex-wrap:nowrap


### flex-flow ###

shorthand for flex-wrap & flex-direction

gelijkaardig aan padding: 


## Grid ##

system for responsive design

a row always contains "12 column units"

every screen from 1200px will have col-lg
col-md 1200px-900px col-md 
col-sm 900-700px

bootstrap or other frameworks can detect the screensize changing

depending on the class of size

!!!! rows & cols might have margin automaticlly

never on col distrubtion can you set the width (set in the row)
becoz the row will always take 100%;

but below it you put 100%

you can redistribute the column units

## Resources ##

game to learn flexbox
[http://flexboxfroggy.com](http://flexboxfroggy.com)

