Restful API's should be stateless



with Angular best DATA api (easiest & most flexible)

is with a restful api

one that sends and returns same data resource

> JSON Data is easiest


for that data api, Restful is is easiest

- Restul should always be stateless

why not tradional auth

- sessions introduce state
- sessions can't be shared
- gives problems if you want to have multiple servers for the same authentication
- cookies don't flow downstream

## Specific consideration for Angular Apps ##

- route access should be controlled
- need some inidication of authentication
- conditionally show & hide ui elements


## JSON Web Tokens ##

open standard under RFC:

> A Request for Comments (RFC) is a type of publication from the Internet Engineering Task Force (IETF) and the Internet Society, the principal technical development and standards-setting bodies for the Internet.

that describes a way to transmit **claims** between two parties in a **secure** way

transmits jots (JSON Operational Transformation)

- securely transmits claims

- communicate a JSON payload (containing claims)

- claim = information about a subject (mosty the user)

## JOT - JSON Operational Transformation ##

e.g.

	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
	eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.
	TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ

seperated by 3 dots

### so these 3 parts exist out of: ###

	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.

= base64 encoded header

	eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.

= base64 encoded payload

	TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
= signature = 

- encoded header
- encoded payload
- secret

here you can see example of JOT decoder: [https://jwt.io/](https://jwt.io/)


Auth0

authentication service >
authentication broker

we can put all of our users in their database

we send credentials to auth0 and then we recieve a JOT which we use to secure our client/server application