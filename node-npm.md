# NPM project #

## start: npm init ##

this will initialize an npm project.

More specificcally, this will create package.json file

	npm init

## node_modules ##

this folder contains the libraries & frameworks that are required for your project

## package.json ##

This file contains configuration of the **node_modules** to be installed; in other words, tells npm commandline which frameworks & libraries are required to be installed.

The advantage is that you can just copy your project, without **node_modules**, which will often have a big size and can be later installed.

## npm install [framework] --save ##

This command will do two things:

- install angular in node modules
- state the dependency on this framework inside package.json

cmd:

	
	npm install angular --save


## retrieve required frameworks from package.json ##

simple!

cmd:

	npm install



