# Angular 1.X #

Client 

- kan browser zijn
- of bv applicaties zoals Postman

Server


typical flow client - server

in geval van browser:

- browser vraagt request naar server (naar bv index.html)
- server gaat bestand opzoeken in zijn filesysteem
- stuurt bestand terug naar de browser
- dan gaat de browser van regel 1 sequentiëel alle regels parsen
	- bv als hij lijn tegenkomt op css in te laden, laadt hij dat in het moment dat hij die lijn tegenkomt
	- als hij dan script tag tegenkomt, vraagt hij JS bestand aan, gaat EERST code uitvoeren en DAN PAS rest van code uitlezen

## Verschillende manieren functies aan te initializeren ##

no hoisting & anonieme functie

	var func = function(){}

wel hoisting

	function func(){}

## Single Page Applications ##

bij traditional page applications, telkens opnieuw laden van html, including js, css files

TOV

bij SPA wordt er 1 pagina gemaakt en dan telkens adhv ajax protocol wordt dom gemanipuleerd


## Directives ##

die maken Angular wat Angular is

#### pascalCase dash conventions ####

custom tags maken!!

bv:

<alert-me></alert-me>

moet gedefinieerd worden als

kan zowel als element als attribute gespecifiëerd worden


## Views ##

## Controllers ##

controllers mogen niet te veel logica bevatten maar moeten hun logica door delegeren naar services

zodat je de code van verschillende services kan gebruiken op verschillende controllers

modulair werken

## Services ##

- bevatten de echte domein logica

- bv CRUD API exposen naar controllers toe

- zijn altijd allemaal singletons


## Modules ##

- modulariteit
- opzichzelf staand project die geen externe dependencies heeft
- modules bevatten een API om van alle onderdelen (services, controllers, directives) registratie te doen


## $scope function ##

bevat functie $watch

gaat kijken wanneer er waarde is veranderd, en op het moment dat er een andere waarde is gebeurd, kan je daar op reageren

#### gebruik enkel lightweight functies bij watches ####


## $promise ##

- publish - subscriber pattern
- prevents the UI from freezing
	- "call me when read, i do something else in the meantime"
	- it's an object where you set callback in case of success & in case of failure


## structuur ##

service heeft directe contact met API / Data Model

controller gaat service aanroepen & de verkregen data doorgeven aan de scope


## Material Design ##

- material palette om kleurenschema's te vinden binnen Material Design
- d
