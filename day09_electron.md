# Electron #

## structure ##

 no frameworks needed (javascript)

why?

- speed
- exact control (so you know exactly what happens)
- framework churn (?)

we don't need responsive design because it's a desktop application!!

so mostly we will work with absolute design

## conventions ##

menu / content view

-> make use of the golden ratio

[https://en.wikipedia.org/wiki/Golden_ratio](https://en.wikipedia.org/wiki/Golden_ratio)

this is used to calculate the ideal distance of the distance from main content to the navigation

the main content to the top bar

the distance of the icons from the left & right side from the side nav bar


## constant reload of application ##

	require("electron-reload");

 in main.js but doesn't work on windows!!

also always delete the dependencies, if you start from a existing project



## absolute layout (css)##

we want to exactly control how the layout will look like.  Since mostly the application will be run on desktop so for the main content for example, we'd like:

   	position: absolute;
    left:65px; //width of the sidenavigation
    top:30px; //height of the topnavigation
    bottom:0px;
    right:0px;

& there will be no problem with fitting everything on the screen we'll mostly work with:

 	min-width: 500px;


## View Engine ##

we need to use a view engine, otherwise when we navigate between different parts of the application will blink

there's different options, amongst others:
	- handlebars
	- jade

### handlebars ###

to install:

	npm install handlebars --save

prerequisite is also underscore so:

	npm install underscore --save


### view.js ###

to read the filesystem

	var fs = require('fs');

	var path = require('path');
	var HandleBars = require('handlebars');
	
the idea is to make a single page & then render the different views in to the same page

	var View = function(viewName){

so here we create the path of the view from the viewName + hbs extension (can be any extension of your choosing)

you can choose your own convention but follow it

	    var templatePath = path.join(__dirname, "../views", viewName + ".hbs");
we read the source from the file system

	    var source = fs.readFileSync(templatePath, "utf-8");

we compile the .hbs file at runtime

	    var template = HandleBars.compile(source);

then we insert the container with the compiled page
	    
		this.toHtml = function(data)
	    {
	        return template(data);
	    };
	    
    
	}

//module is from node.js

	module.exports = View;


## filesystem to write ##

needed to write delete edit files

	npm install fs-extra --save 