# JavaScript basics CTG #

## == not equal (value) ##

bv
	
	(0 == false) //true
	('' == false) //true



=== not equal (type/value)

	(0 === false) //false want integer & boolean niet zelfde type
	('' === false) //false want string & boolean niet zelfde type


## Best practice changing images: ##
(best performance, especially for mobile) 

    var myImage = document.querySelector('img');

    myImage.onclick = function () {
        var mySrc = myImage.getAttribute('src');
        if (mySrc === 'images/firefox-icon.png') {
            myImage.setAttribute('src', 'images/ctg-logo.jpg');
        } else {
            myImage.setAttribute('src', 'images/firefox-icon.png');
        }
    }

## MomentJS ##

Data manipulation & formatting


## Dom Manipulation ##


your application only runs as fast as your slowest DOM Manipulation

some libraries:

### jQuery ###
### React.JS ###
### MooTools ###

don't overuse selectors, store in variables when possible


## Graphical / Data visualisation ##

### D3 ###
very low level
### Three.JS ###
check it, cool


## GUI Related ##

try to use reusable components

to make Single Page Applications possible (among other reasons)

### AngularJS ###
### Bootstrap ###
### Kendo UI ###
### jQuery UI ###
### React ###

## Template Systems ##

stel een element dat je meerdere keren gebruikt > je wil dat niet telkens opnieuw schrijven

check Mustache, HandleBars, JSX

## Unit Testing ##

#### bij het kiezen van een framework/library, kijk naar grootte van communicty ####
#### hoe groter community, hoe meer documentatie & samples ####

### Jasmine ###
### Mocha ###
### QUnit ###
### Unit.js ###

## Application Structuring ##

hoe je code te organizeren

 controllers, directives, services 

Volg de structuur - best practice van het framework

### AngularJS ###
belangrijkste

er zijn er nog veel


## Toolbelt libraries ##

Javascript leuk maar heel omslachtig om meerdere keren te doen

grote tip:

#### maak als developer altijd een startproject waarbij je telkens mee begint als je AngularJS project start ####

met alle verschillende libraries & frameworks dat je wil gebruiken

why use toolbelt libraries?

- iterating over Arrays, Objects, Strings
- can have consistency (of performance) in older browers
- more....

## More Options ##

### UnderScore.JS ###

### Lo-Dash ###

CHECK IT OUT - easier syntax
