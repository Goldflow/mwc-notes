## *ngIf directive ##

you can equal this to a condition, only if the condition is met, the DOM inside this dom element will display

this will only execute if ***ngIf** condition returns **true** or **truthy** in this case

	*ngIf="selectedHero"

	
for example

	  <div *ngIf="selectedHero">
	  <h2>{{selectedHero.name}} details!</h2>
	  <div><label>id: </label>{{selectedHero.id}}</div>
	  <div>
	    <label>name: </label>
	    <input [(ngModel)]="selectedHero.name" placeholder="name"/>
	  </div>
	</div>