## Flexbox ##

.flex-container

items negeren hun standaard css en kunnen ook inline worden afhankelijk van grootte scherm

### flex-direction ###
row : autoamtisch van links naar rechts
row-reverse: automatisch alle elementen van rechts naar links

    flex-direction:row-reverse

column: automatisch van boven naar beneden, ongeacht van hun display properties
column-reverse: omgekeerd

	flex-direction:column


### flex-wrap ###

hou de grootte zodat alle items in er in passen, zonder de items aan te passen

	flex-wrap:wrap

de items aanpassen zodat ze passen in de flex-container

	flex-wrap:nowrap


### flex-flow ###

shorthand for flex-wrap & flex-direction

gelijkaardig aan padding: 


## Grid ##