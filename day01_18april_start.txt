Linting
-------

soort van stramien enforcen (juist omd at in javascript je alles kan/mag)
dus bepaalde conventies vastleggen


Taskrunner
---------

compilen van 1e taal naar andere taal
bv less/sass naar css
bv typedscript naar javascript


Node.js
-------

javascript aan server side 
> supersnel
> zeer schaalbaar


npm: node packaging modules
---------------------------
- makkelijk om javascript modules in te pompen in uw project
- npm install [libraryname] (vanuit commandline)


Bower
-----
ook soort van packaging module (tegenhanger NPM)
ook vanuit commandline


start:

git clone "repository url"

navigate to folder

npm install
 //automaticcly install all libraries, scripts, frameworks defined in package.json
 //could also possibly start up a server if this is defined in the package.json file

npm start
//should start server 
//more concrete: will execute the line defined next to 'start' in the package.json e.g.: "http-server -a localhost -p 8000 -c-1"

//if not installed >
npm install -g live-server

//to start running the app on an (already installed) http-server
//while in the corerct folder type:
lite-server