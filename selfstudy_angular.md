# various notes JavaScript & Angular #

## Best Practices: ##

http://www.w3schools.com/js/js_best_practices.asp


## Compile Service $compile ##

Compiles an HTML string or DOM into a template and produces a template function, which can then be used to link scope and the template together.

but you use this typiccally only in a directive

## Parse Service $parse ##

Converts Angular expression into a function.

but you use this typiccally only in a directive

## Locale Service $locale ##

in html:

  	<div ng-controller="LocaleSampleController" style="padding:20px;">

        <h3>{{myDate | date:myFormat}}</h3>

    </div>

include script:

	<script src="/js/controllers/LocaleSampleController.js"></script>

include the locale you want to use (get it from: [https://code.angularjs.org/](https://code.angularjs.org/) )

	<script src="/lib/angular/angular-locale_nl.js"></script>

code inside the LocaleController:

	'use strict';

	eventsApp.controller('LocaleSampleController',
	    //injecting $locale service to use $locale datetime format
		function LocaleSampleController($scope, $locale) {
	
		    $scope.myDate = Date.now();
		    //myFormat is set to full date property of locale date time format
		    $scope.myFormat = $locale.DATETIME_FORMATS.fullDate;

	})

## TimeOut service $timeout ##

add a delay before executing things
using a promise so you can possibly cancel the delay

similar to JavaScript's native setTimeOut function
except $timeout will happen inside "the Angular world" so that the bindings will be reevaluated automatticaly
using setTimeOut could possibly cause for the changes done insite not to occur because they happen outside of the scope of Angular

## Cookie Store service $cookiestore ##

in scripts:

	<script src="/lib/angular/angular-cookies.js"></script>

