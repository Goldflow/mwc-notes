## Routing ##

basiccly:

in JavaScript define ngRoute service


    // create the module and name it scotchApp
        // also include ngRoute for all our routing needs
    var scotchApp = angular.module('scotchApp', ['ngRoute']);

than create configure the routes:

    // configure our routes
    scotchApp.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'pages/home.html',
                controller  : 'mainController'
            })

            // route for the about page
            .when('/about', {
                templateUrl : 'pages/about.html',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/contact', {
                templateUrl : 'pages/contact.html',
                controller  : 'contactController'
            });
    });


than write a seperate controller for every page:

 	// create the controller and inject Angular's $scope
    scotchApp.controller('mainController', function($scope) {
        // create a message to display in our view
        $scope.message = 'Everyone come and see how good I look!';
    });

    scotchApp.controller('aboutController', function($scope) {
        $scope.message = 'Look! I am an about page.';
    });

    scotchApp.controller('contactController', function($scope) {
        $scope.message = 'Contact us! JK. This is just a demo.';
    });




## Angular structure ##

#### state & authorisatie ####

bij het routing kan je dan bv beveiliging inbouwen:

allen als user juiste credentials/rechten heeft routen naar andere pagina


#### app.module.js ####

apart bestand, kan in grote applicaties veel extra modules zijn dat je er bij inlaad en dan is dat beter apart



## Angular testing ##

je moet angular-mocks adden in je project

angular-mocks nodig om volgende functies/services onder andere:

	$controller

om controllers te mocken

	$rootScope

om een blank scope te kunnen maken
(scope te kunnen mocken)

	module() 

functie om de app te mocken



## Best Practices ##
- DOM manipulation code ENKEL in directives
- vermijd gebruik van $scope.$watch
	- (gebruik expliciet met commando's)

-used named functions, controllers, directives

bv: elk component die service binnenpakt moet ngInject erboven staan hebben:

op die manier geen probleem bij minifien

want anders kan 't zijn dat een service renamed wordt naar 'az' terwijl er geen controller zal bestaan

	/*@ngInject*/
	someModule.controller('MyController', function($scope, greeter) {
	  //ngInject zorgt ervoor dat hij metadata gaat opslaan
		//en dat et geen problemen gaat geven bij gebruiken van services
	});
	
		