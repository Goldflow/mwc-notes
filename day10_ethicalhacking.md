# Ethical Hacking #

check nog es: [http://ceukelai.re/](http://ceukelai.re/)

WebGoat goed om te oefenen + filmkes met oplossingen

zoek up eens CEH

## OWASP ##

Open Web Application Security Project

standaard elk jaar top 10 meest voorkomende security problemen

(er zijn nog andere standaarden maar dit is een goeie, relatief eenvoudige voor bedrijven "aware" te maken)

goeie awareness website

### elke 3 jaar nieuwe top 10 ###

standaard elk jaar top 10 meest voorkomende security problemen

vroeger meest voorkomende maar dit was geen juiste voorstelling (veel bedrijven, vooral banken, willen niet dat dit buitenkomt)

nu gesorteerd op meest risico

### huidige: 2013 ###

de mobiele apps zijn meer en meer dichter aan het komen tot web applicaties

dus we gaan focussen op de web top 10


### OWASP heeft chapters in meeste landen ###


die van belgie: [https://www.owasp.org/index.php/Belgium](https://www.owasp.org/index.php/Belgium)

volgende 23 mei

## Praktische hulp van OWASP ##

- er zijn OWASP libraries voor alle talen
- verschillene tastbare tips op cheat sheets
	- bv.: [https://www.owasp.org/index.php/SQL_Injection_Prevention_Cheat_Sheet](https://www.owasp.org/index.php/SQL_Injection_Prevention_Cheat_Sheet)

## A1 Avoiding Injection Flaws ##

avoid by:

- prepared statements/stored procedures
- ...
- perform **whitelist** input validation: niets toelaten, behalve als het op whitelist staat vs **blacklist**: alles toelaten behalve wat niet op blacklist staat


## B2 Broken Authentication & Session Management ##

### http stateless protocol ###

- credentials moeten met elke request meegaan
- SSL should be used for EVERYTHING requiring authentication
	- BUT latest TSL standards must be applied to server + client on everything

### session management flaws ###
- make sure session id is not exposed on:
	- network
	- browser
	- logs

### beware side-doors ###

change password, remember password, forgot my password, secret question, logout, email address

make sure it cannot be easily retrieved using **social engineering**

### broken authentication ###

bv sessionid uithalen (!!! zet niet in url parameters)

www.boi.com?JSESSIONID=9FA1DB9EA ...

### verify architecture ###

use proven architecture e.g. Auth0

### verify implementation ###

see slides:
check owasp tools > **WebScarab**

for exercies: **WebGoat**

verify logoff actually destroyed

### cheat sheet: ###

https://www.owasp.org/index.php/Authentication_Cheat_Sheet

## A3 Cross-Site Scripting (XSS) ##

bv javascript:alert(document.cookie)

works!!! is possible to be sent to other domain

you go to other website with web parameters with session id inside 

- don't include user supplied input in output page
- use ESAPI > contains functions encode/decode your input
- use white list input validation
- use OWASP's AntiSamy to sanitize html

## A4 Insecure Direct Object References ##

- nooit verbergen van authorised objects > just do not load them in dom
- never regulate server side authorisation on client!!!
- always assume clientside is unsafe because everything can be modified on clientside
- prevent url-parameters

### eliminate direct object references ###

for example 

	http://app?file=Report123.xls

 should become 

	http://app?file=1

### validate the direct object reference ###

- alles wat van user komt, moet je valideren!! dus ook checken

- alles wat je terugstuurt, moet je ook sanitizen dus ook checken

- query constraints per user!!

## A5 Security Misconfiguration ##

- set admin paswoord!
- alle juiste poorten gesloten
- not same password developer & production
- source code, make sure it doesn't stay on accessible places!!!!
- always change all credentials in production

### security linux: KaliLinux, vol tools voor "ethical" hacking ###
 gaat onder andere kijken naar software versies op servers en welke patches/versies missing zijn en welke vulnerabilities dan vrij zijn om te misbruiken

### gelijkaardige tool: Nasus ###

## A6 Sensitive Data Exposure ##

- failure to identify sensitive data
	- wetgeving belgie: alles dat persoon kan identificeren
	- maar gaat verder

### niets is echt beveiligd binnen bedrijfsnetwerk met firewall ###
dus bv als je overschrijft binnen bedrijfsnetwerk kunnen ze alles zien

### ook cheat sheet ###

## A7 Missing Function Level Access Control ##

### architecture ###
- use simple, positive model at every layer: must be done during design phase, not as a fix: show what's needed but not more
- make sure you have a security mechanism at every layer

### unauthorised requests: ZAP ###

ZAP is a free tool (one among some others) to forge unauthroized requests (paid tools are generally better & more extensive)


## A8 - Cross Site Request Forgery (CSRF) ##

- bv s wegdoen van https > blijft werken
- vulnerability caused by browsers automaticcally including user authentication data

## A9 - Using Known Vulnerable Components ##

### don't always used the newest ###

be up to date about vulnerabilities from frameworks/libraries

## A10 Unvalidated Redirects ##

## check slides Summary: how do you address these problems? ##

### print the cheat sheets etc. ###

### Learn to develop secure code ###


## General Advice ##

### hou rekening met insider attacks!!! ###

### gebruik standaarden!!! ###
use used & proven methodes/standaarden om te beveiligen.
begin niet je eigen encryptie te schrijven

### never show too much information ###

### put yourself in mindset of hacker ###

### cross-frame scripting ###

in frame van echte website maar dan inside frame hackerwebsite

oplossing > FRAMEBUSTING

## logging & auditing ##

- logging belangrijk maar niet te veel informatie
- teveel logging en er niet naar kijken (en geregeld)
- hackers gaan liefst logs aanpassen om sporen uit te wissen

## tips persoonlijk paswoord ##

maak 1 heel sterk paswoord (random cijfers letters, uppper-lower case & speciale karakters)

dan voor elk bedrijf waar ej inlogt, refactor je de bedrijfsnaam in je paswoord op zelfde maniers

