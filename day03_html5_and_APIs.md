# HTML5 #
## HTML5 some new tags ##

    <input type="email" name="email" required >
    
required needed or will not check for required pattern

    <input type="tel">
    
you can also use regexp > http://www.w3schools.com/tags/att_input_pattern.asp

`autofocus="autofocus"` > automaticly put focus here when rendering page

`autocomplete="on/off"` > helps user complete forms based on earlier input


##HTML5 api's##



###HTML5 Geolocation###

automaticcly already in standard javascript present

navigator.geolocation

with navigator.getWatch you can keep tracking the user

    <script>
    var x = document.getElementById("demo");
    function getLocation() {
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
    } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
    }
    }
    function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude; 
    }
    </script>



###HTML5 WebRTC###

you can get access to video & microphone!!!!

check: 


[http://ctgweb.azurewebsites.net/Home/WebRTC](http://ctgweb.azurewebsites.net/Home/WebRTC)

###HTML5 WebStorage###

window.localStorage > (semi)permanent 

window.sessionStorage > only during session

###HTML5 WebWorkers###
for running threads in the background, to do heavy work without having your browser blocking

see [http://www.w3schools.com/html/html5_webworkers.asp](http://www.w3schools.com/html/html5_webworkers.asp)

to reuse a worker > 

`var myWorker = new Worker('anUrl');`

Before creating a web worker, check whether the user's browser supports it:

    if(typeof(Worker) !== "undefined") {
    // Yes! Web worker support!
    // Some code.....
    } else {
    // Sorry! No Web Worker support..
    }

Now, let's create our web worker in an external JavaScript.

Here, we create a script that counts. The script is stored in the "demo_workers.js" file:

    var i = 0;
    
    function timedCount() {
    i = i + 1;
    postMessage(i);
    setTimeout("timedCount()",500);
    }
    
timedCount();
The important part of the code above is the postMessage() method - which is used to post a message back to the HTML page.

Note: Normally web workers are not used for such simple scripts, but for more CPU intensive tasks.

###HTML5 Offline Web Apps###

you can keep certain pages in cache (index.html) 
and also in a fallback in case of offline: offline.html

defined in Manifest file

you have to in your html the `<html manifest="[name].manifest">`


###HTML5 Notification###

[https://developer.mozilla.org/en/docs/Web/API/notification](https://developer.mozilla.org/en/docs/Web/API/notification "https://developer.mozilla.org/en/docs/Web/API/notification")

###HTML5 Web Sockets###

still experimental but allows for permanent connection
(for video stream or chat programs)

[http://www.tutorialspoint.com/html5/html5_websocket.htm](http://www.tutorialspoint.com/html5/html5_websocket.htm)

###HTML5 Server-Sent Events###

as with other api's first check if api is supported (EventSource)
then create a new EventSource, will listen to a page

serverside
 have to send evens tream
in header content-type: 
text/Event-stream
cache-control:no cache

response to be sent:

expires=-1 //doesn't expire


###HTML5 Other API's###

Battery Status

History so you can go back

Drag&Drop

Navigation Timing 
> good for testing!! the time it needs to render 