# Security Mobile/Web Development #

Olivier heel geinteresseerd in alles ivm Application Lifecycle Management


## Software Application Architecture ##

- process of defininng structured solution that meets requirements:

	- functional
	- technical
	- operational

	while also optimizing **quality attributes**

- tradeoff/balance between **competing requirements:**

	- user
	- system(it)
	- business goals



## Quality Attributes ##

#### Design Qualities ####

- maintability: componenten met weinig dependency
- reusability: (ook weer zelfde verhaal)
- conceptual integrity

#### Run-time Qualities ####

- availability (depending on SLA)
- interopability : in what way it must together with other applications
- ... see slides

#### System Qualities ####

- supportability
- testability
- usability

#### User Qualities ####
- availibity

## Crosscutting Concerns ##

- common functionality that spans layers & tiers
- should be centralized in one location in the code, where possible
- typical supports operations such as:

Authentication, Authorisation, and more see slides

## Security ##

- capacity of reducing chance of malicious or accidental actions outside of desgined usage: affecting system & disclosure or loss of information

- increate reliability of system

- protect assets & prevent unauthorized access

### main aspects: ###

- Authentication
- Authorisation
- Encryption
- Auditing
- Logging


## Authentication ##

- check if someone is who they claim to be

- check if there's a clear seperation of concerns & responsabilities; 

	- e.g. user view in View Layer, business logic in Business Layer
	
	- also for example putting View Layer on one server, business on another & data on a third!

- enforce use of STRONG passwords (regular forced change)

- consider **single sign on strategy** (SSO)

- NEVER transmit/store passwords in plain text

## Authorisation ##

process of giving permission to get access

- consder resource-based authorization for system auditing

- consider using role-based authorisation for business decisions

- limit amount of roles: maintanability, testability, etc.

- consider **claims based authorisation**
	- additional layers of abstraction
	- support Federated Authorization: helpt u om single sign on te doen, er is een trust tussen verschillende systemen > adv trust die je hebt krijg je een claim binnen van een bepaald systeem 


## Claims-based Identity & Access Control ##


1. user asks for authentication at issuer 
2. gets issue token from issues
3. sends token to application
<hr>
#### how to: ####


- the information doesn't need which identity stored
- eliminate duplicate accounts
- factor authentication logic out of your applications
- use more granular permission than roles might provide: je kan meer of minder macht geven op basis van de user info

### 3 types of claims ###

- reserved
	- prefedined ones
	- not mandatory but recommended (experation date etc.)
- public
- private claims

DON'T PUT ROLES IN CLAIM IF POSSIBLE!!!


### Claim ###
- statement that a subject makes about itself or another subject:
	- name
	- identity
	- group
	- privilege
	- capability
	
...etc

- often key-value pairs
- properties:
	- claim name
	- claim type
	- claim value


### Security Token  ###

- sent over connection between client & issuer
- provides strong proof of 
	- integrity of claim
	- identiy of user

### Issuer  ###

(can be server or for example Auth0)


- client requests authentication
- issues gathers information with .e.g. active directory
- issues returns token to client
- client sends token to claims-based application


## Implementing Claims-Based Identiy ##

1. AddLogic to your applications to support claims:
	- know how to validate the incoming security token & how to parse the containing claims
2. acquire or build an issuer
3. configure your application to trust the issuer
4. configure the issuers to know about the application
	- when configuring token, you can also encrypt the data inside the token!! for even more security

**use SSL (https) for both issues & application** (against eavesdroppers)

**for stronger security, always request encrypted tokens**

- but it's a tradeoff on performance
- both issues & application have certificate but application also private key



see slides for more complete



### Passive mode (negotiation) - claims based identity & access control ###

check slides

hier pas een token gegeven nadat het aangevraagd is

relies more on browser redirects >

- http get, post TO request
-  pass around tokens

### Active mode ###

**automatisch of manueel programmatorisch eerst al token aanvragen**

**voor de rest zelfde manier als passive mode**

voor dat je service gaat gebruiken, weet je al zelf dat je aan die webservice een token gaat moeten geven en die service gaat consumeren

- dus eerst geeft client aan issuer token om claims aan te vragen
- issuer geeft dan token met claims terug
- nadat de client de claims heeft gekregen, gaat hij call doen op service (op data die gevraagd is) ook weer met de token
- dan gaat issuer de call volbrengen als juiste claims zijn volbracht


eenmaal je token hebt van issuer, kan diezelfde token gebruikt worden tijdens de levenscyclus voor die applicatie


### External Issuers ###

bv je hebt vertrouwen in 1 van de external issuers (bv facebook, twitter, github)

- your application may accept security tokens created by other well-known issuers
- your internal issuer: such as ADFS (or AzureACS) can then accept a security token from an issuer in another realm (external issuer acts as another ADFS store) as proof of authentication


### Forms of tokens ###

####  Security Assertion Markup Language (SAML Token) ####

####  Json Web Token ####

[www.jwt.io](www.jwt.io)

advantages jwt saml

	- json size smaller
	- json parsers common in most languages
	- used at internet scale, ease of client side processing on multiple platforms (mobile)


## JSON Web Token (jwt) ##

## JOT - JSON Operational Transformation ##

e.g.

	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
	eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.
	TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ

seperated by 3 dots

### so these 3 parts exist out of: ###

	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.

= base64 encoded header

	eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.

= base64 encoded payload

	TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
= signature = encrypted from encryption defined in encrypted header:

- encoded header
- encoded payload
- secret

here you can see example of JOT decoder: [https://jwt.io/](https://jwt.io/)



## Open standard for Authorization - OAuth ##

- log in to third party accounts using their accounts without exposing their password
- OAuth2.0 not backwards compatible with OAuth1.0
- OAuth2.0 focuses on client developer simplicity while providing specific authentication flows