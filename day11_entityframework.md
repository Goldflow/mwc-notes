##  Lazy Loading ##

#### virtual keyword in c# > lazy loading: ####

> automatically query & populate the contents of these values/functions
whenever I accesss them (otherwise they remain null)

### Olivier advice; gebruik geen lazy loading: ###

maar liever complete controle over data access

dan bv:

	getCustomer
	
	getCustomerWithAddress
	
	getCustomerWithBookingDetails
	
	getCustomerWithAddressAndPricing
	
	...

echter at wordt veel werk, om dan toch complete controle te hebben over welke data je terugkrijgt; kan je volgende design pattern gebruiken:

	getCustomer(Customer, customerId);

waar je bv de subObjecten bv (Address, Pricing and/or BookingDetails) enkel zal ophalen, als die **not null** zijn.

Die subobjecten komen dan ook vaak overeen met hun eigen entities

bv:

	c = new Customer(new Address);
	getCustomer(c,customerId);

dan krijg je customer terug met de hoofdgegevens + ook address maar geen pricing of bookingdetails !




## Seperate Data Access in 2 layers (Olivier) ##

repository deel:
logische laag : entry punt van data access,
object teruggeven aan client die zegt van waar data komt

2e laag: fysische laag: specifieke link naar bron, kan van verschillende bronnen komen
je moet aan aggregatie doen



## Entity Framework BaseTypes ##

### DbContext ###
DbContext is a basetype from Entity Framework
Context represents a session with the database
and allows us to query & save data

### DbSet ###
Allows us to query & save instances of those data types

## CodeFirst Migration ##

### what and why ###
as our application evolves over time, we'll need to make changes to our domain model > and that in turn means we need to make changes to our database

to do this, we'll make use of a feature called: **CodeFirst Migrations**

### how to add ###
enable this by opening:
tools > Package Manager Console >

	Enable-Migrations

### result ###

enabling CodeFirst Migrations adds:

- a configuration class
	- folder migrations get added to
	- register providers for 3rd party databases
	- specify CData, stands for character data: [https://www.google.be/webhp?#q=cdata](https://www.google.be/webhp?#q=cdata "stands for character data") - [http://stackoverflow.com/questions/2784183/what-does-cdata-in-xml-mean](http://stackoverflow.com/questions/2784183/what-does-cdata-in-xml-mean "see also here")

### adding migrations ###

now to keep changes to the model structured and organized and also so you can individually manage them, you can add different changes as a "migration" like so:

(also in Package Manager Console
you can see PM>)

	PM> Add-Migration AddUrl

this could be for example for adding a get/set method fo url variable in class Blog

then don't forget to do:

	PM> update-database

to complete the migration!

## change db config - alternative conventions ##

2 options:

- **Data annotations**
- **Fluid API**

### Data Annotations ###

	using System.ComponentModel.DataAnnotations;

adding *[Key]* will inform system that below value should be the key

	 public class User
	    {
	        [Key]
	        public string UserName { get; set; }
	        public string DisplayName { get; set; }
	    }
to apply this:

	PM> Add-Migration AddUser

than
	PM> Updata-Database

### Fluid API ###


for example if you want the column of *DisplayName* of the user to be called *display_name* instead of *DisplayName*

in the class BlogContext for example:

	 public class BlogContext : DbContext
	    {
	        /*
	         * DbSet:
	         Allows us to query & save instances of those data types
	         */
	
	        public DbSet<User> Users { get; set; }
		}

you can add the following in the context file:

	protected override void OnModelCreating(DbModelBuilder modelBuilder)
	        {
            modelBuilder.Entity<User>()
                 .Property(u => u.DisplayName)
                 .HasColumnName("display_name");
        	}

this code have the desired effect (as described before)

you can add anything you like in the OnModelCreating function



## Setup ##
Before you can use Entity Frameworks: don't forget to create a new MSSQL database connection:

(LocalDb)\MSSQLLocalDB

# start PluralSight ASP.NET #

[https://app.pluralsight.com/player?course=aspdotnet-5-ef7-bootstrap-angular-web-app&author=shawn-wildermuth&name=aspdotnet-5-ef7-bootstrap-angular-web-app-m1&clip=1&mode=live](https://app.pluralsight.com/player?course=aspdotnet-5-ef7-bootstrap-angular-web-app&author=shawn-wildermuth&name=aspdotnet-5-ef7-bootstrap-angular-web-app-m1&clip=1&mode=live)

## introduction ##

### ASP.NET 5 = complete rethinking of the platform ###

- cross platform & open source
- single platform (MVC & WebAPI combined)
- evertyhing is a dependency - you have complete control of what you load
- low memory footprint
- multiple deployment support:
	- cloud, IIS, Self-Hosting etc.

### CLR ###

Common Language Runtime:  the virtual machine component of Microsoft's .NET framework, manages the execution of .NET programs

[https://en.wikipedia.org/wiki/Common_Language_Runtime](https://en.wikipedia.org/wiki/Common_Language_Runtime)

### Overview ###

- can be hosted both on Linux & Windows

- new .NET Core (contains CoreCLR that runs both on Windows & Linux)

- 3 frameworks:
	- .NET 4.6 > windows
	- Mono > Linux
	- .NET Core : cross platform .NET Framework

- CoreCRL subset of natural .NET Framework

- everything is a NuGet Package: everything is optional
- embraces open web development
	- npm tooling support
	- bower for client-side library support
	- grunt/gulp for build automation
	- NuGet for .NET packages
	- everything optional/none of this above is required


## DNX Dot Net Execution variable ##

a command that points at some project and knows how to run that code

but needs a project.json file
e.g.

	{
	    "frameworks": {
	        "dnx451":{}
	    }
	}
> to verify dnx version to put in to project.json you can do dnx run (probably other commands also exist

e.g.

	dnx run

(inside the folder and will run the file program.cs)

## starting ASP.NET 5 app in VS.Code + CMD ##

### Yeoman ###
standard included in npm now

install aspnet-generator

	npm install aspnet-generator -g


this will open Yeoman & give you options

	yo

this will start up ASP.NET 5 generator:

	yo aspnet

then you can choose with arrows what kind of project you want to start but since node sometimes has problems with the arrows, use 1 to ... to be sure correct choice is added

we choose web application here

this will create a project with the desired name

navigate to it & type 

	 code .

to open it in Visaul Code

### Restore ###

if you open the Startup.cs, it will complain > you click on restore 

or commandline:
	dnu restore

#### why? ####

- this will pull in all required NuGet packages for us (like *npm install*)
- give us intellisense :)


## running ASP.NET 5 from commandline ##

to run the project based on the variables configured in *project.json* type:

	dnx run

in the output you will also find at the end, the port it's running on

# starting ASP.NET 5 project Visual Studio 2015 ##


## start ##

1.  new project
2.  ASP.NET Web application
3.  ASP.NET 5 empty (normally you take Web Applications that already has a boilerplate)
4.  tick off run in azure cloud
5.  create

## Structure ##

in the class *Startup.cs*:

#### void Main ####
- entry point: *void Main* of this class will :
> "run it using an instance of the StartUp class"
just for startup of project

#### ConfigureServices ####
handles depedency containers

als je JsonData teruggeeft

conventies:

beginnen de verschillende members altijd met hoofdletter

als je niets meegeeft aan formatter zal hij alles doorgeven met hoofdletter

-> als het met camelCase moet, moet je dat in ConfigureServices meegeven

bv door
binnen useMVC:

	 options.SerializerSettings.ContractResolver()

#### void Configure ####

> what things are going to be called as a request comes in, and as a response goes back out

- use app.UseIISPlatformHandler(): als je bv via andere manier served dan IIS dan heb je dat niet nodig natuurlijk
> to run via IIS (similar to web / lite-server)


- after entry point, configure will be ran
	- for example what must be done on output & input
	- how to handle requests/responses

- useMVC 
	- RESTful url serving

- useStaticFiles()
	- statische, bestaande files ook serven


e.g.
        public void Configure(IApplicationBuilder app)
        {
	         app.Run(async (context) =>
            {
				//this will print out the path requested on localhost
                await context.Response.WriteAsync($"Hello World m0f0! path:{context.Request.Path}");
            });
        }

# Entity Framework 7 #

the core ASP.NET doesn't know about any data providers so you need to add them everytime

### writing our data providers ###

for example:
	
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using Microsoft.Data.Entity;
	
	namespace TheWorld.Models
	{
	    public class WorldContext: DbContext
	    {
	        public DbSet<Trip> Trips { get; set; }
	        public DbSet<Stop> Stops { get; set; }
	
	        //we could also configure this in startup
	        //but we want to WorldContext to be self describing
	        //think modules!
	        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	        {
	            //expects a connectionString which will be setin config.json
	            optionsBuilder.UseSqlServer(connectionString);
	
	            base.OnConfiguring(optionsBuilder);
	        }
	    }
	
	
	}
