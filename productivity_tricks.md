# Productivitiy tricks #

##Brackets: edit multiple lines in ##

ctrl click multiple lines  
than start typing


##Webstorm: edit multiple lines in ##

alt click multiple places (you can use multiple per line)
than start typing

>more on: [https://www.youtube.com/watch?v=JBkGOPR3-nA&feature=youtu.be](https://www.youtube.com/watch?v=JBkGOPR3-nA&feature=youtu.be)

##Webstorm: edit all strings matching selected string  ##

> see: [https://youtu.be/JBkGOPR3-nA?t=123](https://youtu.be/JBkGOPR3-nA?t=123)

basicly:
 alt+j on selection to select the next occurence matching the selected string
you can keep pressing alt+j to select more & more

##Webstorm: edit all strings matching selected string ##

> see: [[https://youtu.be/JBkGOPR3-nA?t=123](https://youtu.be/JBkGOPR3-nA?t=123)](https://youtu.be/JBkGOPR3-nA?t=149)

if you have some kind of dataset (for example, 1 to 10) that you want to copy in place, you can copy the the 1 to 10 (for example with everytime an enter after)
in to for example 10 times
```
<li></li>
```


## Brackets (possibly others) Duplicate previous line ##

ctrl+D

## VS Code + Webstorm: create html template quick: ##

! + tab


## VS Code + Webstorm: create html tag+class quick: ##

- div.className
- press tab


## Visual Studio - properties=getters/setters ##

[http://stackoverflow.com/a/8015467/6209693](http://stackoverflow.com/a/8015467/6209693)

yea type prop and press TAB. Visual Studio has a snippet for automatic property.

For property with public get and private set, you can use propg and press TAB.

For complete non auto property you can use propfull and press TAB.